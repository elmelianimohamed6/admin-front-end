import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ajouter',
  templateUrl: './ajouter.component.html',
  styleUrls: ['./ajouter.component.scss']
})
export class AjouterComponent implements OnInit {

  public types:any=[];
  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit(): void {
    this.http.get("http://localhost:8080/types").subscribe(
      data=>{
        this.types=data["_embedded"].typeEvenements;
      }
    )
  }

  onAddEvent(data:any){
    this.http.post("http://localhost:8080/events",data).subscribe(
      data=>{
        this.router.navigateByUrl("/example");
      },err=>{
        alert("Somethig went wrong !!");
      }
    )
  }

}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Route, RouterModule } from '@angular/router';
import { ExampleComponent } from 'app/modules/admin/example/example.component';
import { AjouterComponent } from '../ajouter/ajouter.component';

const AjouterRoutes: Route[] = [
    {
        path     : '',
        component: AjouterComponent,
    }
];

@NgModule({
    declarations: [
        AjouterComponent
    ],
    imports     : [CommonModule ,FormsModule,
        RouterModule.forChild(AjouterRoutes)
    ]
})
export class AjouterModule
{
}

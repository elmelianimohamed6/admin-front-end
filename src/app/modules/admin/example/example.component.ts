import { HttpClient } from '@angular/common/http';
import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector     : 'example',
    templateUrl  : './example.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ExampleComponent
{
    public events:any=[];
    /**
     * Constructor
     */
    constructor(private http:HttpClient)
    {
    }
    ngOnInit(): void {
        this.http.get("http://localhost:8080/all").subscribe(
          data=>{
            this.events=data;
          }
        )
      }
}

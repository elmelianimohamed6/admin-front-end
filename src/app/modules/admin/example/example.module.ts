import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Route, RouterModule } from '@angular/router';
import { ExampleComponent } from 'app/modules/admin/example/example.component';
import { AjouterComponent } from '../ajouter/ajouter.component';

const exampleRoutes: Route[] = [
    {
        path     : '',
        component: ExampleComponent,
    }
];

@NgModule({
    declarations: [
        ExampleComponent
    ],
    imports     : [CommonModule ,
        RouterModule.forChild(exampleRoutes)
    ]
})
export class ExampleModule
{
}

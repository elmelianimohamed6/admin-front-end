import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-modifier',
  templateUrl: './modifier.component.html',
  styleUrls: ['./modifier.component.scss']
})
export class ModifierComponent implements OnInit {

  public id = this.active.snapshot.params['id'];
  public event: any = [];
  public types: any = [];
  constructor(private http: HttpClient, private router: Router, private active: ActivatedRoute) { }

  ngOnInit(): void {
    this.http.get("http://localhost:8080/types").subscribe(
      data=>{
        this.types=data["_embedded"].typeEvenements;
      }
    )
    this.http.get("http://localhost:8080/event/" + this.id).subscribe(
      data => {
        this.event = data;
      }
    )
  }

  onModifyEvent(data:any){
    this.http.put("http://localhost:8080/modifyEvent/"+this.id,data).subscribe(
      data=>{
        this.router.navigateByUrl("/example");
      },err=>{
        alert("Somethig went wrong !!");
      }
    )
  }

}

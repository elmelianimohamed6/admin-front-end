import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { Route, RouterModule } from '@angular/router';
import { AjouterComponent } from '../ajouter/ajouter.component';
import { ModifierComponent } from './modifier.component';

const ModifierRoutes: Route[] = [
    {
        path     : '',
        component: ModifierComponent,
    }
];
 
@NgModule({
    declarations: [
        ModifierComponent
    ],
    imports     : [CommonModule ,FormsModule,
        RouterModule.forChild(ModifierRoutes)
    ]
})
export class ModifierModule
{
}
